##The docker environment for Cloud9 SDK based on CentOS image

###Build instructions
```shell
$ git clone https://bitbucket.org/armyguy/buildateam-c9sdk.git
$ cd buildateam-c9sdk
$ docker build -t c9sdk ./
```
###Launch instructions
```shell
$ docker run -d -p 80:8181 c9sdk --listen 0.0.0.0 -p 8181 --auth admin:admin
```
###Showing the help for launching the c9sdk
```shell
docker run --rm c9sdk --help
```