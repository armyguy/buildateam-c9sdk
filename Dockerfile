FROM centos

RUN curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -

RUN yum -y install \
    glibc-static \
    nodejs

RUN yum groupinstall -y development

WORKDIR /opt

RUN git clone git://github.com/c9/core.git c9sdk

WORKDIR /opt/c9sdk

RUN scripts/install-sdk.sh

EXPOSE 8181

ENTRYPOINT ["node", "server.js"]
